---
codename: 'onclite'
name: 'Xiaomi Redmi 7'
comment: ''
icon: 'phone'
noinstall: true
maturity: .96
---

An Ubuntu Touch is available. You can find [installation instructions on GitHub](https://github.com/Vin4ter/Ubports-Onclite-redmi7/blob/master/README.md)..