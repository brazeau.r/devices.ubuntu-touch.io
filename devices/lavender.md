---
codename: 'lavender'
name: 'Xiaomi Redmi Note 7'
comment: 'community device'
icon: 'phone'
maturity: .9
---

Xiaomi Redmi Note 7 (released 2019) is one of the [best selling device of 2019](https://en.wikipedia.org/wiki/List_of_best-selling_mobile_phones#2019).

This is the second device port of 2020 maintained by DanctNIX, previously the Redmi 4X (santoni).